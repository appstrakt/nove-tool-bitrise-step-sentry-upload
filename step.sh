#!/bin/bash
PACKAGE_VERSION=$(node -p -e "require('${project_path}/package.json').version")
PACKAGE_NAME=$(node -p -e "require('${project_path}/package.json').name")

if ! grep @sentry "${project_path}/package.json"
then
    echo "Sentry not found in project, no need to continue"
    exit 0
fi

set +e

yarn global add @sentry/cli

VERSION="$PACKAGE_NAME@$PACKAGE_VERSION"

if [[ "${env}" != *production* ]] && [ ! -z "${env}" ]; then
    VERSION+="-${env}"
fi

PROJECT="${project_slug}"

if [ -z $PROJECT ]; then
    PROJECT="${BITRISE_APP_TITLE}"
fi

echo "Uploading version: ${VERSION} to project '${PROJECT}' from build folder: ${build_folder}"

export SENTRY_ORG="${organization}"
export SENTRY_PROJECT=$PROJECT
export SENTRY_AUTH_TOKEN="${auth_token}"

# Create a new release (if release already exists it doesn't effect it)
sentry-cli releases new "$VERSION"

# (Optional) Delete all current files (sourcemaps and sourcecode) for this release
sentry-cli releases files "$VERSION" delete -A

# Upload all sourcemaps inside the build folder and prefixes it with ~/static/js.
# More info: https://docs.sentry.io/cli/releases/#sentry-cli-sourcemaps
sentry-cli releases files "$VERSION" upload-sourcemaps "${build_folder}/static/js" \
      --url-prefix '~/static/js' \
      --rewrite \
      --strip-common-prefix

sentry-cli releases files "$VERSION" upload-sourcemaps "${build_folder}/static/chunks" \
      --url-prefix '~/static/chunks' \
      --rewrite \
      --strip-common-prefix


  
# (Optional) Finilize the release
sentry-cli releases finalize "$VERSION"

set -e
